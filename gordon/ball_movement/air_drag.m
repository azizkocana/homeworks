function [ d_a ] = air_drag( t, arg, m, r, rho, c )
% air_drag: computes the air friction applied on the ball with pre definied
% specifications and returns the velocity.
% Args:
%  t(float): time
%  arg(array[float]): 4x1 vector for 2D motion [x, y, vx, vy]
%  args_out_differentiation (static):
%    m(float): weight [kg]
%    r(float): radius of the ball [m]
%    rho(float):
%    v_t(float): terminal velocity [m\s2]
% Return:
%  d_a(array[float]): derivatives 4x1 vector [dx, dy, dvx, dvy]

a = pi * r^2; % [m2] area
g = 9.8;      %[m/s2] gravitational constant

v_x = - (c * rho *a / (2 * m)) * sqrt(arg(3)^2+arg(4)^2) * arg(3);
v_y = - (c * rho *a / (2 * m)) * sqrt(arg(3)^2+arg(4)^2) * arg(4) - g;

d_x = arg(3);
d_y = arg(4);

d_a = [d_x; d_y; v_x; v_y];

end

