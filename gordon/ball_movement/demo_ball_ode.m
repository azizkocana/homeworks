clear all
close all

v_init = 45;               % [m/s] initial velocity
phi = pi/4;                % throw angle of the ball
x = [0; 0];                % [m, m] initial position
g = 9.8;                   %[m/s2] gravitational constant\

m = 0.145;    %[kg] mass
r = 0.0366;   %[m] radius
rho = 1.20;   %[kg/m3] air density
v_t = 42;     %[m/s] terminal velocity

a = pi * r^2; % [m2] area
c = m * g / (rho * a * v_t^2); % drag coefficient


hop_spline = .5;
hop_plot = 5;

v_x = v_init * cos(phi);
v_y = v_init * sin(phi);

time_span = [0 ceil(2 * v_y / g)];
arg = [x ; v_x; v_y];

tic();
[time_45, list_arg_45] = ode45(@(t,ar) air_drag(t,ar, m, r, rho, c),time_span ,arg);
t_ode45 = toc();
disp(strcat('Average time for ode45:',num2str(t_ode45)))

spline_x_45 = (0:hop_spline:max(list_arg_45(:,1)));
spline_y_45 = spline(list_arg_45(:,1),list_arg_45(:,2), spline_x_45);

tic();
[time_23, list_arg_23] = ode45(@(t,ar) air_drag(t,ar, m, r, rho, c),time_span ,arg);
t_ode23 = toc();
disp(strcat('Average time for ode23:',num2str(t_ode23)))

spline_x_23 = (0:hop_spline:max(list_arg_23(:,1)));
spline_y_23 = spline(list_arg_23(:,1),list_arg_23(:,2), spline_x_23);

fig= figure()
subplot(2,1,1)
plot(spline_x_45(1:hop_plot:end), spline_y_45(1:hop_plot:end),'.k','linewidth',1)
hold();
plot(spline_x_23(1:hop_plot:end), spline_y_23(1:hop_plot:end),'-k','linewidth',1)
axis([0 max([spline_x_23, spline_x_45]) 0 max([spline_y_23, spline_y_45])]+2);
xlabel('x[m]', 'fontsize', 12)
ylabel('y[m]', 'fontsize', 12)
legend('ode45','ode23')
box off
print(fig,'demo_ode','-deps')
