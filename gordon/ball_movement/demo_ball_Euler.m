% Computes the trajectory of a thrown ball in a media with air friction.
% Uses Euler's method to compute the changes in velocity as the applied
% force is not constant. (GL-Physics Project Q8)
% Author: Aziz Kocanaogullari - LM: [10/04/18]
% PS: sorry if this version hurts your eyes, just a simple implementation
% did not spend too much time to think on how it should be coded.

clear all
close all

% Parameters
m = 0.145;    %[kg] mass
r = 0.0366;   %[m] radius
a = pi * r^2; % [m2] area

v_init = 45;  %[m/s] initial velocity
phi = pi/4;   %throw angle of the ball

g = 9.8;      %[m/s2] gravitational constant
rho = 1.20;   %[kg/m3] air density
v_t = 42;     %[m/s] terminal velocity

list_rho = [0.98,  1.20]; % [kg/m3] air density for part 2, c = 0 
                          % will be automatically appended.

eps = 10^(-3); % relative tolerance for time determination
hop = 100;      % downsampling on plots on part2, makes figures clearer

%%
% Determine d_t 

fig = figure();
subplot(2,1,1)
hold();
lin_style = {'-k','--.k','-k','--.k','-k','--.k','-k','--.k','-k','--.k'};


c = m * g / (rho * a * v_t^2); % drag coefficient

error = 100;
ymax = 0;
xmax = 0;
idx = 1;

d_t = 3;
while error > eps % continue until tolerance level achieved
      
    v = v_init * [cos(phi), sin(phi)].';
    
    x = [0, 0].';
    
    traj_x = [x(1)];     % movement trajectory in x
    traj_y = [x(2)];     % movement trajectory in y

    while x(2) >= 0,
       
       % Compute net force, average and final velocities and displacement
       % for d_t interval on each axis respectively
       f = - 1/2 * rho * c * a * norm(v) * v + [0, - m * g].'; % net force 
       v_av = v + f/m * d_t / 2;                               % average velocity    
       v = v + f/m * d_t;                                      % final velocity

       x = x + v_av * d_t;                                     % displacement
       
       % Append position to trajectory
       traj_x = [traj_x, x(1)];
       traj_y = [traj_y, x(2)];

    end
    
    % Compute error
    error = abs(max(traj_x) - xmax)/max(traj_x);
    
    % Plot all trajectories obtained
    plot(traj_x, traj_y, lin_style{idx}, 'linewidth', 1)
    ymax = max(max(traj_y), ymax);
    xmax = max((traj_y(end)>0) * traj_x(end) + (traj_y(end)<0) * traj_x(end-1), xmax);
    idx = idx+1;
    d_t = d_t / 3;    % logarithmic grid search for the d_t value
       
end

% Put axis labels and etc.
disp(strcat(strcat(strcat('Iterations ended with d_t:',num2str(d_t)),'[s] with error:'),num2str(error)))
axis([0 xmax 0 ymax+2]);
xlabel('x[m]', 'fontsize', 12)
ylabel('y[m]', 'fontsize', 12)
title('Trajectories with decreasing d_t', 'fontsize', 12)

%%
% Compute trajectories of 3 environments
subplot(2,1,2)
hold();
lin_style = {'ok', '--*k', '--+k', '--k'};
idx = 1; 

% Compute drag coefficients for different media
for idx = 1:length(list_rho),
    c = m * g / (rho * a * v_t^2); % drag coefficient
    list_c(idx) = c;
end
list_c = [0 ,list_c];
list_rho = [1, list_rho];

t_hold = [];

for idx = 1:length(list_c),
    
    c = list_c(idx);
    rho = list_rho(idx);

    v = v_init * [cos(phi), sin(phi)].';
    x = [0, 0].';
    
    traj_x = [x(1)];
    traj_y = [x(2)];
    
    tic();
    while x(2) >= 0,

       % Compute net force, average and final velocities and displacement
       % for d_t interval on each axis respectively
       f = - 1/2 * rho * c * a * norm(v) * v + [0, - m * g].'; % net force 
       v_av = v + f/m * d_t / 2;                               % average velocity    
       v = v + f/m * d_t;                                      % final velocity

       x = x + v_av * d_t;                                     % displacement

       % Append position to trajectory
       traj_x = [traj_x, x(1)];
       traj_y = [traj_y, x(2)];
    end
    t_elapsed = toc();
    t_hold = [t_hold, t_elapsed];

    plot(traj_x(1:hop:end), traj_y(1:hop:end), lin_style{idx}, 'linewidth', 1)
    ymax = max(max(traj_y), ymax);
    xmax = max((traj_y(end)>0) * traj_x(end) + (traj_y(end)<0) * traj_x(end-1), xmax);
end

disp(strcat('Average time for Euler method:',num2str(mean(t_hold))))

% Analytic result
 v = v_init * [cos(phi), sin(phi)].';
 time_scale = v(2) / 9.8 * 2;
 
x_1 = v(1) .* [0:0.001:time_scale];
x_2 = v(2) .* [0:0.001:time_scale] - g / 2 .* [0:0.001:time_scale].^2;
plot(x_1, x_2, 'k', 'linewidth', 1)

axis([0 xmax 0 ymax+2]);
xlabel('x[m]', 'fontsize', 12)
ylabel('y[m]', 'fontsize', 12)
title('Trajectories of the ball in different media', 'fontsize', 12)
legend('no friction','CFD','sea', 'analytic')
print(fig,'air_friction','-deps')



