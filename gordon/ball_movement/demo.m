clear all
close all

list_phi = pi / 180 * [10:1:70]; % throw angle of the ball            
hop_v = .1; % [m\s] grid parameter for velocity search
g = 9.8; %[m\s2] gravitational constant

hop_spline = .5;
hop_plot = 5;

list_title = {'LF-FP','RCF-FP','RCF-CF'};
list_wall_d = [94.5, 114.3, 114.3];
list_wall_h = [11.2, 1.82, 1.82];
list_rho = [1.20, 1.20, 0.98];

m = 0.145;    %[kg] mass
r = 0.0366;   %[m] radius
v_t = 42;     %[m/s] terminal velocity

a = pi * r^2; % [m2] area
c = m * g / (list_rho(1) * a * v_t^2); % drag coefficient

param = [m,r,list_rho(1),c].';

for idx = 1:3
    param(3) = list_rho(idx);
    wall_d = list_wall_d(idx);
    wall_h = list_wall_h(idx);
    [min_v, phi] = airfric( wall_d, wall_h, list_phi, hop_v, param );    
    
    opt_v{idx} = min_v;
    opt_phi{idx} = phi;
    
    x = [0; 0]; % [m, m] initial position
    g = 9.8; %[m/s2] gravitational constant\

    v_x = min_v * cos(phi);
    v_y = min_v * sin(phi);

    time_span = [0 ceil(2 * v_y / g)];
    arg = [x ; v_x; v_y];

    tic();
    [time_45, list_arg_45] = ode45(@(t,ar) air_drag(t,ar, m, r, list_rho(idx), c),time_span ,arg);
    t_ode45 = toc();
    disp(strcat('Average time for ode45:',num2str(t_ode45)))

    spline_x_45 = (0:hop_spline:max(list_arg_45(:,1)));
    spline_y_45 = spline(list_arg_45(:,1),list_arg_45(:,2), spline_x_45);
    
    spline_x{idx} = spline_x_45;
    spline_y{idx} = spline_y_45;
    
    
end


fig = figure();
for idx = 1:3;
    wall_d = list_wall_d(idx);
    wall_h = list_wall_h(idx);
    
    spline_x_45 = spline_x{idx}(spline_y{idx}>=0);
    spline_y_45 = spline_y{idx}(spline_y{idx}>=0);
    
    subplot(3,1,idx)   
    plot(spline_x_45(1:hop_plot:end), spline_y_45(1:hop_plot:end),'--.k','linewidth',1)
    hold();
    plot([wall_d wall_d], [0 wall_h],'color','black','linewidth', 4);
    xlabel('x[m]', 'fontsize', 12)
    ylabel('y[m]', 'fontsize', 12)
    legend('ball','wall')
    title(list_title{idx},'fontsize', 12)
    box off
    disp(strcat(strcat(strcat(strcat(list_title{idx},' velocity - angle : '),num2str(opt_v{idx})),' - '),num2str(opt_phi{idx}/pi*180)))
    
end

print(fig,'demo_wall','-deps')