function [ min_v, min_ang ] = airfric( wall_d, wall_h, list_phi, hop_v, param )
% airfric: computes the minimum angle and minimum initial velocity required
% for a home run defined by wall distance and a wall height where the
% origin is initial position of the thrown ball. A basic implementation of
% 2D grid search.
% Args:
%   wall_d(float): distance of the wall in [m]
%   wall_h(height): height of the wall in [m]
%   list_phi(array(float)): list of angles to be spanned in [rad]
%   hop_v(float): specificity of the velocity grid search [m\s2]
% Return:
%   min_v: minimum velocity [m\s2]
%   min_ang: minimum velocity angle [rad]

g = 9.8; %[m\s2] gravitational constant

m = param(1);
r = param(2);
rho = param(3);
c = param(4);

list_v = [];
wb = waitbar(0,'Computing Velocities.');
idx_wb = 0;

for phi = list_phi
    
    abs_v = 1; % [m/s] initial velocity
    while 1
    
        x = [0; 0];                % [m, m] initial position   
        v_x = abs_v * cos(phi(1)); % [m/s] speed on x direction
        v_y = abs_v * sin(phi(1)); % [m/s] speed on y direction

        time_span = [0 ceil(2 * v_y / g)];
        arg = [x ; v_x; v_y];
        [time_45, list_arg_45] = ode45(@(t,a)air_drag(t,a, m, r, rho, c),time_span ,arg);

        est_h = spline(list_arg_45(:,1),list_arg_45(:,2), wall_d);
        est_x = [wall_d; est_h].';

        if est_h > wall_h
            list_v = [list_v; abs_v];
            break;
        else
           abs_v = abs_v + hop_v; 
        end
    end
    
    idx_wb = idx_wb + 1;
    waitbar(idx_wb / length(list_phi),wb,'Computing Velocities.');
    
end

close(wb)

idx_min = find(list_v == min(list_v));
idx_min = idx_min(1); % select one if multiple indices are found

min_v = list_v(idx_min); % [m\s]
min_ang = list_phi(idx_min); % [rad]

end

